package main

import (
	"encoding/json"
	"errors"
	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
	"github.com/google/uuid"
	"log"
	"quint.as/awsutils"
	"quint.as/iptracker"
	"time"
)

// Handler is your Lambda function handler
// It uses Amazon API Gateway request/responses provided by the aws-lambda-go/events package,
// However you could use other event sources (S3, Kinesis etc), or JSON-decoded primitive types such as 'string'.
func Handler(request events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {
	// stdout and stderr are sent to AWS CloudWatch Logs
	log.Printf("Debug: ++Handler()")
	defer log.Printf("Debug: --Handler()")

	log.Printf("Info: Processing Lambda request %s\n", request.RequestContext.RequestID)

	postData := request.Body

	if len(postData) == 0 {
		log.Printf("Error: post data was missing")
		return awsutils.GenerateProxyResponse("{\"error\": \"post data missing\"}", 400)
	}

	log.Printf("Info: Post body: " + postData)

	var ipData iptracker.Record

	err := json.Unmarshal([]byte(postData), &ipData)

	if err != nil {
		log.Printf("Error: failed to unmarshal JSON: " + err.Error())
		return awsutils.GenerateProxyResponse("{\"error\": \"failed to unmarshal JSON: "+err.Error()+"\"}", 400)
	}

	theUuid, err := uuid.NewRandom()

	if err != nil {
		log.Printf("Warning: failed to generate UUID: " + err.Error())
	}

	theUuidStr := theUuid.String()

	ipData.Guid = theUuidStr

	theTime := time.Now()
	timeStr := theTime.String()

	ipData.Timestamp = timeStr

	ipData.IsMostRecentRecord = true

	err = updateDb(ipData)

	if err != nil {
		log.Printf("Error: failed to write item to DynamoDB: " + err.Error())
		return awsutils.GenerateProxyResponse("{\"error\": \"failed to write item to DynamoDB\"}", 500)
	}

	return awsutils.GenerateProxyResponse("{\"success\": true}", 200)
}

func updateDb(r iptracker.Record) error {
	log.Printf("Debug: ++updateDb()")
	defer log.Printf("Debug: --updateDb()")

	ses, err := session.NewSession()
	svc := dynamodb.New(ses)

	// check if this record is different from the previous one
	isDuplicateRecord := false

	prevRecord, err := iptracker.GetMostRecentDbRecordForHost(r.Host)

	if err != nil {
		log.Printf("Error: wasn't able to check most recent record in DB: " + err.Error())
		return errors.New("wasn't able to check most recent record in DB")
	}

	if prevRecord.Host == r.Host && prevRecord.PrivateIp == r.PrivateIp && prevRecord.PublicIp == r.PublicIp && prevRecord.VpnIp == r.VpnIp {
		isDuplicateRecord = true
		log.Printf("Info: this record is the same as the previous one")
	} else {
		log.Printf("Info: this record is different than the previous one")
	}

	av, err := dynamodbattribute.MarshalMap(r)
	if err != nil {
		log.Printf("Error: failed to marshal map to DynamoDB attribute: " + err.Error())
		return err
	}

	// Create item in dynamodb table
	input := &dynamodb.PutItemInput{
		Item:      av,
		TableName: aws.String("ip_tracker"),
	}

	_, err = svc.PutItem(input)
	if err != nil {
		log.Printf("Error: failed calling PutItem: " + err.Error())
		return err
	}

	log.Printf("Info: Successfully updated database with new record")

	if isDuplicateRecord {
		err := iptracker.DeleteRecordWithGuid(prevRecord.Guid)

		if err != nil {
			log.Printf("Error: failed to delete record with guid " + prevRecord.Guid + ": " + err.Error())
			return errors.New("failed to delete record with guid " + prevRecord.Guid)
		}
	} else {
		err := setIsMostRecentRecordFalseForGuid(prevRecord.Guid)

		if err != nil {
			log.Printf("Error: failed to set isMostRecentRecord = false for guid " + prevRecord.Guid + ": " + err.Error())
			return errors.New("failed to set isMostRecentRecord = false for guid " + prevRecord.Guid)
		}
	}

	return nil
}

func setIsMostRecentRecordFalseForGuid(guid string) error {
	log.Printf("Debug: ++setIsMostRecentRecordFalseForGuid()")
	defer log.Printf("Debug: --setIsMostRecentRecordFalseForGuid()")

	ses, err := session.NewSession()
	svc := dynamodb.New(ses)

	input := &dynamodb.UpdateItemInput{
		ExpressionAttributeValues: map[string]*dynamodb.AttributeValue{
			":i": {
				BOOL: aws.Bool(false),
			},
		},
		TableName: aws.String("ip_tracker"),
		Key: map[string]*dynamodb.AttributeValue{
			"guid": {
				S: aws.String(guid),
			},
		},
		ReturnValues:     aws.String("UPDATED_NEW"),
		UpdateExpression: aws.String("set isMostRecentRecord = :i"),
	}

	_, err = svc.UpdateItem(input)
	if err != nil {
		log.Printf("Error: failed to update item with guid " + guid + ": " + err.Error())
		return errors.New("failed to update item with guid " + guid)
	}

	return nil
}

func main() {
	lambda.Start(Handler)
}
