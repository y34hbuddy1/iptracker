all: build package

build: ipTracker.go
	GOOS=linux go build ipTracker.go

package: ipTracker
	zip ipTracker.zip ipTracker
